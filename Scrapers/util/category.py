""" Category List description """

def get_category(name):
    parent = None
    
    if name is None:
        return None
    name_low = name.lower().strip()

    if name_low in ["saree", "sarees", "full collections"]:
        category = "Sarees"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["lehenga choli", "lehenga", "choli"]:
        category = "Lehengas"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["salwar kameez", "salwar suit", "salwars"]:
        category = "Salwar Suit"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["kurti", "kurtis"]:
        category = "Kurti"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["gowns", "gown"]:
        category = "Gowns"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["bangles", "bangle"]:
        category = "Bangles"
        parent = "Jewellery"
        return { "category": category, "parent": parent }

    elif name_low in ["dupattas"]:
        category = "Dupattas"
        parent = "Ethnic Wear"
        return { "category": category, "parent": parent }

    elif name_low in ["anklets"]:
        category = "Anklets"
        parent = "Jewellery"
        return { "category": category, "parent": parent }

    elif name_low in ["ear rings", "ear rings", "mangalsutra earrings set", "earrings"]:
        category = "Earrings"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["necklaces", "necklace set"]:
        category = "Necklaces"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["pendants", "pendant"]:
        category = "Pendants"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["rings", "ring", "nose rings"]:
        category = "Rings"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["bracelets", "bracelet"]:
        category = "Bracelets"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["jewellery sets", "jewellery"]:
        category = "Jewellery Sets"
        sub_category = name
        parent = "Jewellery"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["watches", "women watches"]:
        category = "Watches"
        sub_category = name
        parent = "Accessories"
        return { "category": category, "parent": parent, "sub_category": sub_category }

    elif name_low in ["leggings"]:
        category = "Leggings"
        sub_category = name
        parent = "Western Wear"
        return { "category": category, "parent": parent, "sub_category": sub_category }
    return None





    

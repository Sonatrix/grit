from datetime import datetime as dt
import scrapy
import uuid
from django.utils.text import slugify
from items import Product

class HeadphoneIndiaSpider(scrapy.Spider):
    name = "headphonesindia"
    sender = "Headphone Zone"
    start_urls = [
        'https://www.headphonezone.in/collections/best-seller',
        'https://www.headphonezone.in/collections/wireless-bluetooth-headphones',
        'https://www.headphonezone.in/collections/wireless-bluetooth-earphones',
        'https://www.headphonezone.in/collections/in-ear-earphones',
        'https://www.headphonezone.in/collections/noise-cancellation',
        'https://www.headphonezone.in/collections/extra-bass'
        ]

    def parse(self, response):
        # follow links to author pages
        for product in response.css('.product-wrap'):
            href_link = 'https://www.headphonezone.in'+product.css(".hidden-product-link::attr(href)").extract_first()
            yield response.follow(href_link, self.parse_product)

    def parse_product(self, response):

        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["name"] = extract_with_css('h1::text')
        item["storeUrl"] = response.url
        item["old_price"] = float(extract_with_css('.was_price .money::text').replace("₹","").replace(",",""))
        item["price"] = float(extract_with_css('.sale::attr(content)'))
        item["description"] = extract_with_css('[itemprop=description]')
        item["category"] = self.get_category(item["storeUrl"])
        item["images"] = ",".join(response.css(".product_gallery_nav .gallery-cell img::attr(src)").extract())
        item["slug"] = slugify(item["name"])
        item["sender"] = self.name
        brand_name = extract_with_css('[itemprop=brand] a::text')
        if brand_name is None:
            brand_name = item["name"].split("-")[0]
        item["brand"] = brand_name
        
        yield item

    def get_category(self, url):
        category = "Others"
        if "wireless-bluetooth-headphones" in url:
            category = "Wireless Bluetooth Headphones"
        elif "wireless-bluetooth-earphones" in url:
            category = "Wireless Bluetooth Earphones"
        elif "in-ear-earphones" in url:
            category = "In-Ear Phones"
        elif "noise-cancellation" in url:
            category = "Noise Cancellation"
        elif "extra-bass" in url:
            category  = "Extra Bass"
        else:
            return "Others"
        return category

        
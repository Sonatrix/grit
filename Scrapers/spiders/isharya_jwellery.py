from datetime import datetime as dt
import scrapy
import uuid
from django.utils.text import slugify
from items import Product

class IsharyaSpider(scrapy.Spider):
    name = "isharya.com"
    brand = "Isharya"
    start_urls = [
        'https://www.isharya.com/fashion-necklaces/page/1/',
        'https://www.isharya.com/fashion-necklaces/page/2/',
        'https://www.isharya.com/fashion-necklaces/page/3/',
        'https://www.isharya.com/fashion-necklaces/page/4/'
    ]

    def parse(self, response):
        # follow links to author pages
        for href_link in response.css("ul.products .wrap-img::attr(href)").extract():
            yield response.follow(href_link, self.parse_product)

    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["name"] = extract_with_css('h1::text')
        item["storeUrl"] = response.url
        item["old_price"] = float(extract_with_css('.woocommerce-Price-amount::text').replace(",",""))
        item["price"] = float(extract_with_css('.woocommerce-Price-amount::text').replace(",",""))

        item["description"] = extract_with_css('#tab-description')
        item["category"] = "Necklaces"
        item["images"] = ",".join(response.css(".woocommerce-product-gallery__image img::attr(src)").extract())
        item["sender"] = self.name
        item["brand"] = self.brand
        item['code'] = extract_with_css(".sku") 
        
        yield item
        
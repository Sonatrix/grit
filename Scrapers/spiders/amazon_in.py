from datetime import datetime as dt
import scrapy
import uuid
import re
from django.utils.text import slugify
from items import Product

class AmazonSpider(scrapy.Spider):
    name = "amazon_in"
    sender = "Amazon"
    start_urls = [
        'https://www.amazon.in/s/ref=s9_acss_bw_cg_SBCWW_2b1_w?rh=i%3Aapparel%2Cn%3A1571271031%2Cn%3A%211571272031%2Cn%3A1953602031%2Cn%3A11400137031%2Cp_98%3A10440597031%2Cn%3A1968542031&bbn=11400137031&hidden-keywords=-belt%20-sunglass%20-eye&ie=UTF8&pf_rd_m=A1K21FY43GMZF8&pf_rd_s=merchandised-search-4&pf_rd_r=QYVB8JH8W9BJ4X2B188E&pf_rd_t=101&pf_rd_p=5268d3b7-6026-4c1d-be01-ca1942764913&pf_rd_i=11400137031',
    ]

    def parse(self, response):
    	# follow links to author pages
        for product in response.css('#s-results-list-atf .s-item-container'):
            href_link = product.css(".a-row a::attr('href')").extract_first()
            yield response.follow(href_link, self.parse_product)
    
    def parse_product_url(self, response):
        for product in response.css('#pagnNextLink'):
            href_link = product.css(".a-row a::attr('href')").extract_first()
            yield response.follow(href_link, self.parse_product)

    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["name"] = extract_with_css("#productTitle ::text").replace("\n","").strip()
        item["storeUrl"] = response.url+"?tag=zeshastyles-21"
        newPrice = extract_with_css("#priceblock_ourprice::text").strip()
        if newPrice is not None:
            item["price"] = float(re.sub("[^0-9.]", "", newPrice))
        else:
            item['price'] = 0
        oldPriceText = "".join(response.css(".a-text-strike::text").extract())
        if oldPriceText is not None:
            item["old_price"] = float(re.sub("[^0-9.]", "", oldPriceText.strip()))
        else:
            item['old_price']=item["price"]
            

        if item["price"] is None:
            return None

        item["description"] = response.css("#feature-bullets ul").extract_first().replace("\t","").replace("\n","")
        item["category"] = "Tops"
        item["images"] = response.css(".imgTagWrapper img::attr(src)").extract_first()
        item["sender"] = self.name
        item['discount'] = 0
        item['code']= ""
        item["brand"] = extract_with_css("#bylineInfo::text")
        
        yield item
    

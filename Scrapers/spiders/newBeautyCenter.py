from datetime import datetime as dt
import scrapy
import uuid
from django.utils.text import slugify
from items import Product

class BeautyCenterSpider(scrapy.Spider):
    name = "beauty_centre"
    start_urls = [
        'http://www.newbeautycentre.in/skin?page=1',
        'http://www.newbeautycentre.in/skin?page=2',
        'http://www.newbeautycentre.in/skin?page=3',
        'http://www.newbeautycentre.in/skin?page=4',
        'http://www.newbeautycentre.in/hair?page=1',
        'http://www.newbeautycentre.in/hair?page=2',
        'http://www.newbeautycentre.in/hair?page=3',
        'http://www.newbeautycentre.in/makeup?page=1',
        'http://www.newbeautycentre.in/makeup?page=2',
        'http://www.newbeautycentre.in/makeup?page=3',
        'http://www.newbeautycentre.in/makeup?page=4',
        'http://www.newbeautycentre.in/makeup?page=5',
        'http://www.newbeautycentre.in/makeup?page=6',
        'http://www.newbeautycentre.in/abs-pro'
    ]

    def parse(self, response):
        # follow links to author pages
        for product in response.css('.product-list'):
            href_link = product.css(".product-thumb .image a::attr('href')").extract_first()
            yield response.follow(href_link, self.parse_product)

    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["name"] = extract_with_css('h1::text')
        item["storeUrl"] = response.url+"?tracking=5bb91ca2e2ccb"
        item["old_price"] = float(extract_with_css('[itemprop="price"]::text').replace("₹",""))
        item["price"] = item["old_price"]

        item["description"] = extract_with_css('[itemprop="description"]')
        if "abs-pro" in response.url:
            item["category"] = "Nail Paint"
        elif "hair" in response.url:
            item["category"] = "Hair Care"
        else:
            item["category"] = "Facewash & Cleanser"
        item["images"] = extract_with_css('.product-info .image a::attr(href)')
        item["sender"] = self.name
        item["brand"] = extract_with_css("[itemprop='brand']::text")
        
        yield item
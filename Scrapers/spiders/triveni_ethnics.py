from datetime import datetime as dt
import scrapy
import uuid
import re
from django.utils.text import slugify
from items import Product
from bs4 import BeautifulSoup
from util.category import get_category

class TriveniSpider(scrapy.Spider):
    name = "triveni_ethnics"
    brand = "Triveni Ethnics"
    start_urls = [
        'https://www.triveniethnics.com/category/sarees.html',
        'https://www.triveniethnics.com/category/lehanga-choli.html',
        'https://www.triveniethnics.com/category/salwar-kameez.html',
        'https://www.triveniethnics.com/category/kurtis.html',
        'https://www.triveniethnics.com/category/gowns.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/bangles.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/bracelets.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/ear-rings.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/mangalsutra-earrings-set.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/necklaces.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/necklace-set.html',
        'https://www.triveniethnics.com/category/accessories/jewellery/pendants.html',
        'https://www.triveniethnics.com/category/full-collections.html'
    ]
    
    def parse(self, response):

        # for category in response.css(".block-content .nav-item"):
        #     href = category.css("a::attr('href')").extract_first()
        #     yield scrapy.Request(href, callback = self.parse) 
        # follow links to author pages
        category = response.css('.category-title h1::text').extract_first().strip()
        if category != "SALE":
            for product in response.css('h2.product-name'):
                href_link = product.css("a::attr('href')").extract_first()
                follow_link = response.follow(href_link, self.parse_product)
                follow_link.meta['category'] = category
                yield follow_link

        next_page = response.css('.toolbar-bottom li.next a::attr(href)').extract_first()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["code"] = extract_with_css('.sku .value::text')
        item["name"] = extract_with_css('[itemprop=name]::text').strip()
        item["storeUrl"] = response.url+"?acc=a3c65c2974270fd093ee8a9bf8ae7d0b"
        newPrice = extract_with_css('span.special-price .price::text')
        if newPrice is not None:
            item["price"] = float(re.sub("[^0-9.]", "", newPrice.replace("Rs.", "").strip()))
        else:
            item['price'] = 0
        oldPriceText = extract_with_css('span.old-price .price::text')
        if oldPriceText is not None:
            item["old_price"] = float(re.sub("[^0-9.]", "", oldPriceText.replace("Rs.", "").strip()))
        else:
            item['old_price']=item["price"]   

        if item["price"] is None:
            return None

        descriptionText = extract_with_css('.product-data-div .table')
        soup = BeautifulSoup(descriptionText, 'lxml')
        soup.caption.decompose()
        item["description"] = str(soup)
        category = get_category(response.meta['category'])
        if category is not None:
            item["category"] = category['category']
            item["parent"] = category['parent']

        item["images"] = extract_with_css(".product-image #zoom-btn::attr(href)")
        item["sender"] = self.name
        item['discount'] = int(re.sub("[^0-9.]", "", extract_with_css(".discount_percentage::text")))
        item["brand"] = self.brand
        
        yield item
    

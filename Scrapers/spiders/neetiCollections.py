from datetime import datetime as dt
import scrapy
import uuid
import re
from django.utils.text import slugify
from items import Product

class NeetiSpider(scrapy.Spider):
    name = "neeti_collections"
    brand = "Neeti Collections"
    start_urls = [
        #'https://www.neeticollections.com/Kurtis',
        'https://www.neeticollections.com/index.php?route=product/special&page=1',
        'https://www.neeticollections.com/index.php?route=product/special&page=2',
        'https://www.neeticollections.com/index.php?route=product/special&page=3',
        'https://www.neeticollections.com/index.php?_route_=Kurtis/Work-Wear&page=1',
        'https://www.neeticollections.com/index.php?_route_=Kurtis/Work-Wear&page=2',
        'https://www.neeticollections.com/index.php?_route_=Kurtis/Work-Wear&page=3',
        'https://www.neeticollections.com/index.php?_route_=Kurtis/Work-Wear&page=4',
        'https://www.neeticollections.com/index.php?_route_=Kurtis/Work-Wear&page=5', 
        'https://www.neeticollections.com/Lehengas'
    ]

    def parse(self, response):
        # follow links to author pages
        for product in response.css('.product-item-container'):
            href_link = product.css("h4 a::attr('href')").extract_first()
            yield response.follow(href_link, self.parse_product)

    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first() 
        item = Product()
        item["id"] = uuid.uuid4()
        item["name"] = extract_with_css('h1::text')
        item["storeUrl"] = response.url+"&tracking=5a9c08c9c30f1"
        newPrice = extract_with_css('span.price-new::text')
        if newPrice is not None:
            item["price"] = float(re.sub("[^0-9.]", "", newPrice))
        else:
            item['price'] = 0
        oldPriceText = extract_with_css('span.price-old::text')
        if oldPriceText is not None:
            item["old_price"] = float(re.sub("[^0-9.]", "", oldPriceText))
        else:
            item['old_price']=item["price"]
            

        if item["price"] is None:
            return None

        item["description"] = extract_with_css('#tab-1')
        item["category"] = "Kurtis"
        item["images"] = response.css("[itemprop=image]::attr(src)").extract_first()
        item["sender"] = self.name
        item['discount'] = int(re.sub("[^0-9.]", "", extract_with_css(".label-percent")))
        item['code']= response.css(".product-box-desc .model::text").extract_first().replace("Product Code:", "").strip()
        item["brand"] = self.brand
        
        yield item
    

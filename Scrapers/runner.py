import scrapy
from scrapy.utils.project import get_project_settings

from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from spiders.amazon_in import AmazonSpider
from spiders.headphonesindia import  HeadphoneIndiaSpider
from spiders.heels_and_shoes import HealsAndShoesSpider
from spiders.isharya_jwellery import IsharyaSpider
from spiders.newBeautyCenter import BeautyCenterSpider
#from spiders.neetiCollections import NeetiSpider
from spiders.triveni_ethnics import TriveniSpider


settings = get_project_settings()
configure_logging(settings=settings)

runner = CrawlerRunner(settings)
runner.crawl(AmazonSpider)
runner.crawl(HeadphoneIndiaSpider)
runner.crawl(HealsAndShoesSpider)
runner.crawl(IsharyaSpider)
runner.crawl(TriveniSpider)
runner.crawl(BeautyCenterSpider)
d = runner.join()
d.addBoth(lambda _: reactor.stop())

reactor.run() # the script will block here until all crawling jobs are finished
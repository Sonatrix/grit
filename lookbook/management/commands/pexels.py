import re
from django.core.management.base import BaseCommand
from lookbook.models import Image, Category
from django.conf import settings
from pypexels import PyPexels


class Command(BaseCommand):
    """ To fetch list of images from pexels and dump to database """
    args = ''
    help = 'Fetch List of images from pexels'
    def add_arguments(self, parser):
        parser.add_argument('params', nargs='+', type=str)

    def handle(self, **options):
        # instantiate PyPexels object
        py_pexels = PyPexels(api_key=settings.PEXELS_API_KEY)
        category, th = Category.objects.get_or_create(name = "Fashion")
        query = "fashion"
        if options["params"] is not None:
            query = options["params"][0]

        popular_photos = py_pexels.search(per_page=30, query=query)
        while popular_photos.has_next:
            for photo in popular_photos.entries:
                try:

                    title = re.sub("[0-9./-]", " ", photo.url.replace("https://www.pexels.com/photo/", "").strip()).strip().capitalize()
                    slug = re.sub("[/]", " ", photo.url.replace("https://www.pexels.com/photo/", "").strip()).strip()
                    caption = title
                    description = title
                    source = "pexels"
                    external_id = source+"-"+str(photo.id)
                    image = photo.src.get("original")
                    large_url = photo.src.get("large", "")
                    small_url = photo.src.get("small", "")
                    medium_url = photo.src.get("medium", "")
                    tags = [key for key in title.split(" ")] + ["style", "trending", "zeshastyles", query]

                    image, created = Image.objects.get_or_create(
                        external_id=external_id, 
                        defaults={
                        "title": title, 
                        "large_url": large_url,
                        "small_url": small_url, 
                        "tags": tags, 
                        "medium_url": medium_url,  
                        "image": image, 
                        "description": description, 
                        "slug": slug, 
                        "caption":caption, 
                        "source": source, 
                        "category": category
                        })

                except Exception as e:
                    print("error while saving image {0}".format(e))
            # no need to specify per_page: will take from original object
            popular_photos = popular_photos.get_next_page()
            


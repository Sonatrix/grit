from django import template
from lookbook.models import Category, Image

register = template.Library()


@register.inclusion_tag("lookbook/components/popular_images.html")
def show_popular_images():
    images = Image.objects.all().order_by("?")[:5]
    return {"images": images}

@register.inclusion_tag("lookbook/components/list_featured_image.html")
def show_featured_images():
    images = Image.objects.all().order_by("?")[:5]
    return {"images": images}
from django.contrib import admin
from lookbook.models import Image, Category

@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
	list_display = ('title', 'slug', 'image', 'is_public', 'caption')

	list_filter = ('category__name', 'date_added')
	search_fields = ('title', 'description', 'tags')
	prepopulated_fields = {'slug': ('title',)}
	date_hierarchy = 'date_added'
	ordering = ('date_added', )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'slug', 'image',)

	list_filter = ('name', 'date_added')
	search_fields = ('name', 'description', 'tags')
	prepopulated_fields = {'slug': ('name',)}
	date_hierarchy = 'date_added'
	ordering = ('date_added', )

from django.urls import path, include
from lookbook import views
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

app_name = 'lookbook'

urlpatterns = [
    path('', cache_page(CACHE_TTL)(views.ImageListView.as_view()), name='home'),
    path('image/<slug:image>', views.image_detail, name='image_detail'),
]

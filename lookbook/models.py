from django.db import models
import uuid
from django.utils.text import slugify
from meta.models import ModelMeta
from django.utils.timezone import now
from django.contrib.postgres.fields import ArrayField
from django.urls import reverse

from .managers import PhotoQuerySet

class CategoryManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(disabled=False)


class Category(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128, unique=True )
    description = models.TextField(blank=True)

    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)
    objects = models.Manager()
    published = CategoryManager()
    disabled = models.BooleanField(default=False, blank=True)
    image = models.URLField(max_length=255, blank=True, null=True)
    date_added = models.DateTimeField('date added',
                                      default=now)
    _metadata = {
        'title': 'name',
        'description': 'description',
        'image': 'get_image',
        'keywords': 'get_keywords',
        'use_og': "true",
        'url': "get_absolute_url",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Products"
    }

    class Meta:
        db_table = 'image_category'
    
    def get_image(self):
        return self.image

    def get_keywords(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else [self.name]
    
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)

        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('lookbook:category_images', kwargs={'slug': self.slug})

class Image(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField('title',
                             max_length=250)
    slug = models.SlugField('slug',
                            max_length=250,
                            help_text = 'A "slug" is a URL-friendly title for an object.')
    caption = models.TextField('caption',
                               blank=True)
    description = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=25,blank=True, null=True)
    external_id = models.CharField(max_length=25, unique=True)
    date_added = models.DateTimeField('date added',default=now)
    is_public = models.BooleanField('is public', default=True,
                                    help_text= 'Public images will be displayed in the default views.')
    category = models.ForeignKey(
        Category, related_name='images', null=True, on_delete=models.SET_NULL)

    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    image = models.URLField(max_length=255, blank=True, null=True)

    large_url = models.CharField(max_length=255, blank=True, null=True)
    small_url = models.CharField(max_length=255, blank=True, null=True)
    medium_url = models.CharField(max_length=255, blank=True, null=True)
    
    objects = PhotoQuerySet.as_manager()

    class Meta:
        db_table = 'image'
        ordering = ['-date_added']
        get_latest_by = 'date_added'
        verbose_name = "image"
        verbose_name_plural = "images"
    
    _metadata = {
        'title': 'title',
        'description': 'description',
        'image': 'image',
        'url': 'get_absolute_url',
        'tag': 'get_tags',
        'keywords': 'get_tags',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Image"
    }

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.slug is None:
            self.slug = slugify(self.title)
        super(Image, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('lookbook:image_detail', kwargs={'image': self.slug})
    
    def get_tags(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else [self.title]
        
    def image_url(self):
        if self.image:
            return self.image

    def get_large_url(self):
        return self.large_url if self.large_url is not None else self.image

    def get_small_url(self):
        return self.small_url if self.small_url is not None else self.image

    def get_medium_url(self):
        return self.medium_url if self.medium_url is not None else self.image

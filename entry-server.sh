#!/bin/bash

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# python manage.py flush --no-input
# python manage.py migrate
# python manage.py loaddata posts.json

#exec kill -9 $(sudo lsof -t -i:3000)
# Start uvicorn processes
git fetch --all
git pull origin master
python3 manage.py collectstatic --no-input --clear

echo Starting uvicorn
pkill gunicorn
exec gunicorn grit.wsgi:application --bind 0.0.0.0:3000 \
    --workers 4 &

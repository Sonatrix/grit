import uuid
from django.db import models
from meta.models import ModelMeta
from django.contrib.postgres.fields import ArrayField
from django.utils.text import slugify
from django.urls import reverse
from locator.lib.utils import striphtml
from lookbook.models import Category as MainCategory


class CategoryManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(disabled=False)


class Category(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128, unique=True )
    description = models.TextField(blank=True)

    parent = models.ForeignKey(
        'self', null=True, blank=True, related_name='children',
        on_delete=models.CASCADE)

    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)
    objects = models.Manager()
    published = CategoryManager()
    disabled = models.BooleanField(default=False, blank=True)
    image = models.URLField(max_length=255, blank=True, null=True)
    
    _metadata = {
        'title': 'name',
        'description': 'description',
        'image': 'get_image',
        'keywords': 'get_keywords',
        'use_og': "true",
        'url': "get_absolute_url",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Products"
    }

    class Meta:
        db_table = 'category'
    
    def get_image(self):
        return self.image

    def get_keywords(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else [self.name]
    
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)

        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('locator:category_products', kwargs={'slug': self.slug})


class Brand(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    is_company = models.NullBooleanField(default=True, null=True, blank=True)
    slug = models.SlugField(max_length=128,unique=True)
    description = models.TextField(blank=True)
    image = models.URLField(max_length=255, blank=True, null=True)

    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    class Meta:
        db_table = 'brand'

    _metadata = {
        'title': 'name',
        'description': 'description',
        'image': 'image',
        'url': 'get_absolute_url',
        'tag': 'name',
        'keywords': 'name',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Brand"
    }

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)

        super(Brand, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('locator:brands', kwargs={'name': self.slug})

class Product(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    sku = models.CharField(max_length=128)
    description = models.TextField()
    meta_description = models.TextField(default=" ")
    category = models.ForeignKey(
        Category, related_name='products', on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    old_price = models.DecimalField(
        default=0.00, max_digits=10, decimal_places=2)
    store_url = models.URLField(default="")
    slug = models.SlugField(default="", blank=True,
                            unique=True, max_length=128)
    images = ArrayField(
        models.URLField(max_length=400),
        size=30,
        max_length=(400 * 31)  # 6 * 10 character nominals, plus commas
    )
    updated_at = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
    discount = models.DecimalField(
        default=0.00, max_digits=4, decimal_places=2, null=True)
    sender = models.CharField(default="", blank=True, max_length=128)
    brand = models.ForeignKey(
        Brand, related_name='brand', on_delete=models.CASCADE)

    # add tag field for adding keywords related to product
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    class Meta:
        db_table = 'product'

    _metadata = {
        'title': 'name',
        'description': 'get_shortdescription',
        'image': 'get_image',
        'url': 'get_path',
        'tag': 'name',
        'keywords': 'get_keywords',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"get_description",
        "og_type":"Product"
    }

    def __str__(self):
        return self.name
    
    def get_shortdescription(self):
        return striphtml(self.meta_description)

    def get_description(self):
        return striphtml(self.description)[:300]+"..."

    def get_collection(self):
        return self.collection.name

    def get_keywords(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else self.name.split(" ")

    def get_brand(self):
        return self.brand.name

    def get_path(self):
        return "{0}/{1}".format(self.get_category_slug(), self.slug)

    def get_category_name(self):
        return self.category.name

    def get_category_slug(self):
        return self.category.slug

    def get_image(self):
        return self.images[0] if self.images is not None and len(self.images)>0 else None

    def get_discount(self):
        if self.discount is not None:
            return self.discount
        else:
            price_diff = self.old_price - self.price
            return float((price_diff//self.old_price)*100)

    def save(self, *args, **kwargs):
        self.meta_description = self.description[:30] + "..."
        if self.sku in self.name:
            self.slug = slugify(self.name)
        else:
            self.slug = slugify(self.name)+"-"+slugify(self.sku)


        super(Product, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('locator:product_detail', kwargs={'slug': self.category.slug, 'pslug': self.slug})


class CollectionQuerySet(models.QuerySet):
    def get_queryset(self):
        return super().get_queryset().filter(is_published=True)

class Collection(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128, unique=True)
    slug = models.SlugField(max_length=128)
    products = models.ManyToManyField(
        Product, blank=True, related_name='collections')
    image = models.URLField(max_length=255, blank=True, null=True)
    description = models.TextField()
    is_published = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    
    objects = models.Manager()
    published = CollectionQuerySet.as_manager()

    _metadata = {
        'title': 'name',
        'description': 'description',
        'image': 'image',
        'url': 'get_absolute_url',
        'tag': 'name',
        'keywords': 'get_keywords',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Collection"
    }

    class Meta:
        ordering = ['pk']
        db_table = 'collection'

    def __str__(self):
        return self.name
    def get_keywords(self):
        return [self.name]
        
    def get_absolute_url(self):
        return reverse(
            'locator:collection',
            kwargs={'name': self.slug})

    def get_products(self, count=5):
        return self.products.values()[:count]

    def get_image(self):
        return self.image if self.image is not None else None 


class Merchant(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128,unique=True)
    description = models.TextField(blank=True)
    image = models.URLField(max_length=255, blank=True, null=True)
    
    updated_at = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)

    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    class Meta:
        db_table = 'merchant'

    _metadata = {
        'title': 'name',
        'description': 'description',
        'image': 'image',
        'url': 'get_absolute_url',
        'tag': 'name',
        'keywords': 'name',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Merchant"
    }

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)

        super(Merchant, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('locator:merchants', kwargs={'name': self.slug})


class Deal(ModelMeta, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    merchant = models.ForeignKey(
        Merchant, related_name='merchant', on_delete=models.CASCADE)
    category = models.ForeignKey(
        MainCategory, related_name='category', on_delete=models.CASCADE, null=True, blank=True)

    description = models.TextField(blank=True)
    external_id = models.CharField(max_length=128)
    slug = models.CharField(max_length=255, unique=True)
    external_url = models.URLField(max_length=255)
    code = models.CharField(max_length=14, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    created_at = models.DateTimeField(auto_now=True, null=True)
    expiry_at = models.DateTimeField(auto_now=False, null=True, blank=True)

    status = models.CharField(max_length=10, blank=True, null=True)
    terms = models.TextField(blank=True)
    image = models.URLField(max_length=255, blank=True, null=True)
    
    featured = models.BooleanField(default=False, blank=True, null=True)
    # add tag field for adding keywords related to category
    tags = ArrayField(models.CharField(max_length=200), blank=True, null=True)

    class Meta:
        db_table = 'deals'

    _metadata = {
        'title': 'title',
        'description': 'description',
        'image': 'image',
        'url': 'get_absolute_url',
        'tag': 'title',
        'keywords': 'get_tags',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"description",
        "og_type":"Deals"
    }

    def __str__(self):
        return self.title
    

    def get_tags(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else [self.title]

    def save(self, *args, **kwargs):
        self.tags = [self.title, self.category.name]
        if self.slug is None:
            self.slug = slugify(self.title+" "+self.external_id+" "+self.merchant.name)

        if self.image is None:
            self.image = self.merchant.image 

        super(Deal, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('locator:offer_detail', kwargs={'name': self.slug})
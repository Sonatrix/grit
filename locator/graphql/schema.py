# import graphene
# from graphene_django.types import DjangoObjectType
# from locator.models import Product, Brand, Category
# from locator.lib.utils import get_paginator

# class CategoryType(DjangoObjectType):
#     class Meta:
#         model = Category

# class ProductType(DjangoObjectType):
#     class Meta:
#         model = Product

# class BrandType(DjangoObjectType):
#     class Meta:
#         model = Brand

# # Now we create a corresponding PaginatedType for that object type:
# class ProductPaginatedType(graphene.ObjectType):
#     page = graphene.Int()
#     pages = graphene.Int()
#     has_next = graphene.Boolean()
#     has_prev = graphene.Boolean()
#     objects = graphene.List(ProductType)

# class Query(object):
#     category = graphene.Field(CategoryType, id=graphene.String(), slug=graphene.String())
#     product = graphene.Field(ProductType, id=graphene.String(), slug=graphene.String())
#     all_categories = graphene.List(CategoryType)
#     all_products = graphene.List(ProductType)
#     all_brands = graphene.List(BrandType)
#     products = graphene.Field(ProductPaginatedType, page=graphene.Int())


#     def resolve_all_categories(self, info, **kwargs):
#         return Category.published.all()

#     def resolve_all_products(self, info, **kwargs):
#         # We can easily optimize query count in the resolve method
#         return Product.objects.select_related('category').all()

#     def resolve_all_brands(self, info, **kwargs):
#         return Brand.objects.all()

#     def resolve_category(self, info, **kwargs):
#         id = kwargs.get('id')
#         slug = kwargs.get('slug')

#         if id is not None:
#             return Category.objects.get(pk=id)

#         if slug is not None:
#             return Category.objects.get(slug=slug)

#         return None

#     def resolve_product(self, info, **kwargs):
#         id = kwargs.get('id')
#         slug = kwargs.get('slug')

#         if id is not None:
#             return Product.objects.get(pk=id)

#         if slug is not None:
#             return Product.objects.get(slug=slug)

#         return None

#     def resolve_products(self, info, page):
#         page_size = 10
#         qs = Product.objects.all()
#         return get_paginator(qs, page_size, page, ProductPaginatedType)

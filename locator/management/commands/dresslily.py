import csv
import re
from time import mktime
from datetime import datetime
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from django.utils.text import slugify
from locator.lib.utils import safe_html
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from locator.models import Category, Product, Brand
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = 'Fetch list of data from xml'

    def handle(self, **options):
        sender = "Dresslily"
        brand, created = Brand.objects.get_or_create(
           name="Dresslily", defaults={})
        try:
            Product.objects.filter(sender=sender).delete()
            with open(settings.DATA_PATH + '/dresslily.csv', newline='\n') as f:
                reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
                for product in reader:
                    sku = product[0]
                    name = product[1].replace('"',"")
                    url = product[2]+"?lkid=18932699&affiliate_id=10131498"
                    price = re.sub("[^0-9.]", "", product[3])
                    old_price = re.sub("[^0-9.]", "", product[3])
                    print(price)
                    if bool(re.match('^[0-9.]+$', price)):
                        price = int(float(price)*69.65)
                        old_price = int(float(price)*69.65)
                    else:
                        pass
                    print("real", price)
                    images = product[4:6]
                    slug = slugify(name)
                    parent_category = None
                    category = None
                    description = name
                    meta_description = name

                    if product[7] in ["Tops", "Bottoms", "Bikinis", "Plus Size", "One-Pieces", "Dresses", "Intimates"]:
                        parent_category = Category.objects.get(
                            name="Western Wear")
                        category, created = Category.objects.get_or_create(name=product[7],defaults={"parent_id": parent_category.id})
                    elif product[7] == "Jewelry":
                        parent_category = Category.objects.get(
                            name="Jewellery")
                        category, created = Category.objects.get_or_create(
                           name=product[8], defaults={"parent_id": parent_category.id})
                    elif product[7] == "Shoes":
                        parent_category = Category.objects.get(
                            name="Footwear")
                        category, created = Category.objects.get_or_create(
                           name=product[7], defaults={"parent_id": parent_category.id})
                    else:
                        parent_category = Category.objects.get(
                            name="Accessories")
                        category, created = Category.objects.get_or_create(
                           name=product[7], defaults={"parent_id": parent_category.id})
                    try:
                        product = Product.objects.create(name=name, store_url=url, description=description, meta_description=meta_description,
                                                     price=price, old_price=old_price, images=images, slug=slug, sender=sender, category=category, brand=brand)
                        product.save()
                    except Exception as e:
                        print("error while saving product {0}{1}".format(e,product[7]))
        except Exception as e:
            print("error while saving product", e)

# */30 * * * * /usr/bin/python3 /home/manage.py get_post 5

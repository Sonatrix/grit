import untangle
from time import mktime
from datetime import datetime
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from locator.models import Category, Product, Brand
from locator.lib.category import get_category 
from django.conf import settings

#http://indiarush.com/feeds/feed.xml
class Command(BaseCommand):
    args = ''
    help = 'Fetch list of data from xml'

    def handle(self, **options):

        o = untangle.parse(settings.DATA_PATH + '/indiarush.xml')
        product_count = len(o.PRODUCTS.PRODUCT)
        if (product_count > 5):
            sender = "indiarush"
            Product.objects.filter(sender=sender).delete()
        print("Inserting {0} products".format(product_count))

        for item in o.PRODUCTS.PRODUCT:
            try:
                name = item.PRODUCT_NAME.cdata
                url = item.PRODUCT_URL.cdata+"?acc=7137debd45ae4d0ab9aa953017286b20"
                description = item.PRODUCT_DESCRIPTION.cdata
                additional_info = item.ADDITIONAL_INFO.cdata
                price = item.PRODUCT_OFFER_PRICE.cdata
                old_price = item.PRODUCT_PRICE.cdata
                sku = item.PRODUCT_ID.cdata
                category_name = item.PRODUCT_CATEGORY.cdata
                category_details = get_category(category_name)

                if category_details is None:
                    return
                parent = category_details["parent"]
                category_name = category_details["category"]
                parent_id = Category.objects.get(
                    name=parent).id
                
                category, created = Category.objects.get_or_create(
                    name=category_name, defaults={"parent_id": parent_id})
                images = [item.PRODUCT_IMAGE.cdata]
                brand_name = item.MANUFACTURER.cdata
                brand, created = Brand.objects.get_or_create(
                    name=brand_name, defaults={})
                meta_description = BeautifulSoup(
                    description, 'lxml').get_text()
                description += self.prepare_description(additional_info)
                slug = slugify(name)
                product = Product.objects.create(name=name, store_url=url, description=description, meta_description=meta_description, sku=sku,
                                                 price=price, old_price=old_price, images=images, slug=slug, sender=sender, category=category, brand=brand)

                product.save()
                #products.append(product.id)
            except Exception as e:
                print("error while saving product {0}".format(
                    item.PRODUCT_ID.cdata))

            #print(len(products), products)

    def prepare_description(self, data):
        result  = '<ul>'
        for row in data.split("##"):
            if "N/A" not in row and "Buy" not in row:
               result += '<li>'+row+'</li>'

        result += '</ul>'

        return result

# */30 * * * * /usr/bin/python3 /home/manage.py get_post 5

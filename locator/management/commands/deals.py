import csv
import re
from django.utils.dateparse import parse_date
from datetime import datetime
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from django.utils.text import slugify
from locator.lib.utils import safe_html
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from locator.models import Deal, Merchant
from lookbook.models import Category
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = 'Fetch list of data from xml'

    def handle(self, **options):
        sender = "CueLinks"
        try:

            with open(settings.DATA_PATH + '/offers.csv', newline='\n') as f:
                reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
                for product in reader:
                    external_id = product[0]
                    title = product[1].strip()
                    merchant, created = Merchant.objects.get_or_create(
           name=product[2], defaults={})
                    category, created = Category.objects.get_or_create(
           name=product[3], defaults={})
                    description = product[4]
                    terms = product[5]
                    code = product[6]
                    external_url = product[7]
                    status = product[8]
                    created_at = datetime.combine(parse_date(product[9]), datetime.min.time())
                    expiry_at = datetime.combine(parse_date(roduct[10]), datetime.min.time())

                    image = product[12]
                    slug = slugify(title+" "+external_id+" "+ merchant.name)

                    try:
                        deal, created = Deal.objects.get_or_create(
                            external_id=external_id, 
                            defaults={
                            "title": title,  
                            "image": image, 
                            "description": description, 
                            "slug": slug, 
                            "terms": terms, 
                            "code": code, 
                            "external_url": external_url,
                            "status": status,
                            "merchant": merchant,
                            "created_at": created_at,
                            "expiry_at": expiry_at,
                            "category": category
                            })
                        deal.save()
                    except Exception as e:
                        print("error while saving deal {0}{1}".format(e,product[1]))
        except Exception as e:
            print("error while saving product", e)

# */30 * * * * /usr/bin/python3 /home/manage.py get_post 5

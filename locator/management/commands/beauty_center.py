import untangle
from time import mktime
from datetime import datetime
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from django.utils.text import slugify
from locator.lib.utils import safe_html
from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from locator.models import Category, Product, Brand
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = 'Fetch list of data from xml'

    def handle(self, **options):

        o = untangle.parse(settings.DATA_PATH + '/beauty_centre.xml')
        product_count = len(o.items.item)
        if (product_count > 5):
            sender = "beauty_centre"
            Product.objects.filter(sender=sender).delete()
        print("Inserting {0} products".format(product_count))
        parent_category = Category.objects.get(
            name="Beauty Care")
        for item in o.items.item:
            try:
                name = item.name.cdata
                url = item.storeUrl.cdata
                description = safe_html(item.description.cdata)
                price = item.price.cdata
                old_price = item.old_price.cdata
                category_name = item.category.cdata
                category, created = Category.objects.get_or_create(
                    name=category_name, defaults={"parent_id": parent_category.id})
                images = [image for image in item.images.cdata.split(",")[:3]]
                sender = item.sender.cdata
                brand_name = item.brand.cdata
                brand, created = Brand.objects.get_or_create(
                    name=brand_name, defaults={})
                meta_description = BeautifulSoup(
                    description, 'lxml').get_text()
                slug = slugify(name)
                product = Product.objects.create(name=name, store_url=url, description=description, meta_description=meta_description,
                                                 price=price, old_price=old_price, images=images, slug=slug, sender=sender, category=category, brand=brand)
                product.save()
            except Exception as e:
                print("error while saving product {0}".format(item.name.cdata))

# */30 * * * * /usr/bin/python3 /home/manage.py get_post 5

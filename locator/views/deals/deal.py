from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from lookbook.models import Category
from locator.models import Deal, Merchant
from meta.views import Meta
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

class DealsListView(ListView):
    model = Deal
    template_name = 'locator/deals/deals_list.html'
    paginate_by = 10
    meta = Meta(
        title=settings.META_SITE_TITLE,
        url=settings.META_SITE_PROTOCOL + "://" + settings.META_SITE_DOMAIN,
        description=settings.META_DESCRIPTION,
        keywords=settings.META_INCLUDE_KEYWORDS,
        use_og="true",
        use_googleplus="true",
        og_description=settings.META_DESCRIPTION,
        og_type="Offers",
        extra_props={
            'viewport': 'width=device-width, initial-scale=1.0, minimum-scale=1.0'
        },
        extra_custom_props= [
            ('http-equiv', 'Content-Type', 'text/html; charset=UTF-8'),
        ]
    )
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        list_exam = Deal.objects.all()
        paginator = Paginator(list_exam, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            file_exams = paginator.page(page)
        except PageNotAnInteger:
            file_exams = paginator.page(1)
        except EmptyPage:
            file_exams = paginator.page(paginator.num_pages)

        context['offers'] = file_exams
        context['meta'] = self.meta
        return context

@cache_page(CACHE_TTL)
def offer_detail(request, name):
    """ get deal detail based on slug """
    offer = get_object_or_404(Deal, slug=name)

    return render(request, 'locator/deals/deals_detail.html', {'offer': offer, 'meta': offer.as_meta()})

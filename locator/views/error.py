from django.shortcuts import render, redirect

def handler404(request, exception):
    """ 404 Not Found Error """
    return render(request,'locator/error/404.html',{})

def handler500(request):
    """ %00 Server error """
    return render(request,'locator/error/500.html', {})
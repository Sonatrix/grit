# Generated by Django 2.1.1 on 2018-09-15 07:43

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locator', '0016_auto_20180902_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='images',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.URLField(max_length=255), max_length=7905, size=30),
        ),
    ]

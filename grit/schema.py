import graphene
from  locator.graphql.schema import Query as locatorQuery

class Query(locatorQuery, graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

schema = graphene.Schema(query=Query)
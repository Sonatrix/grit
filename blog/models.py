from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.urls import reverse
from meta.models import ModelMeta
from django.utils.text import slugify
from datetime import datetime

class PublishedManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().prefetch_related().filter(status='published')


class Post(ModelMeta, models.Model):
    STATUS_CHOICES = (('draft', 'Draft'), ('published', 'Published'))
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='blog_posts')

    body = models.TextField()
    short_description = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tags = ArrayField(models.CharField(max_length=250), blank=True, null=True)
    external_url = models.URLField(default="", blank=True, null=True)
    is_top = models.NullBooleanField(default=False, null=True, blank=True)

    objects = models.Manager()
    published = PublishedManager()
    is_featured = models.NullBooleanField(default=True, null=True, blank=True)
    status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, default='draft')

    category = models.ForeignKey('locator.Category', on_delete=models.CASCADE)

    images = ArrayField(
        models.URLField(max_length=255, blank=True, null=True),
        size=10, blank=True, null=True,
        max_length=(255 * 11)  # 6 * 10 character nominals, plus commas
    )
    sender = models.CharField(max_length=255, default="shoppstar")

    class Meta:
        ordering = ('-publish',)
        db_table = 'post'

    _metadata = {
        'title': 'title',
        'description': 'short_description',
        'image': 'get_image',
        'url': 'get_absolute_url',
        'tag': 'title',
        'keywords': 'get_keywords',
        'use_og': "true",
        "use_googleplus": "true",
        "og_description":"short_description",
        "og_type":"Article"
    }

    def __str__(self):
        return self.title

    def get_category_name(self):
        return self.category.name

    def get_category_slug(self):
        return self.category.slug

    def get_image(self):
        return self.images[0] if self.images is not None and len(self.images)>0 else None

    def get_keywords(self):
        return self.tags if self.tags is not None and len(self.tags)>0 else [self.title]

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[self.publish.year, self.publish.month, self.publish.day, self.slug])

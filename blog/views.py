from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from blog.models import Post
from locator.models import Category
from meta.views import Meta
from django.conf import settings

class PostListView(ListView):
    model = Post
    template_name = 'blog/post/post_list.html'
    paginate_by = 10
    meta = Meta(
        title=settings.META_SITE_TITLE,
        url=settings.META_SITE_PROTOCOL + "://" + settings.META_SITE_DOMAIN,
        description=settings.META_DESCRIPTION,
        keywords=settings.META_INCLUDE_KEYWORDS,
        use_og="true",
        use_googleplus="true",
        og_description=settings.META_DESCRIPTION,
        og_type="Article",
        extra_props={
            'viewport': 'width=device-width, initial-scale=1.0, minimum-scale=1.0'
        },
        extra_custom_props= [
            ('http-equiv', 'Content-Type', 'text/html; charset=UTF-8'),
        ]
    )
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['posts'] = Post.published.all()
        context['meta'] = self.meta
        context['parent_category'] = Category.published.all().filter(parent=None)
        return context


def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug=post,
                             status='published',
                             publish__year=year,
                             publish__month=month,
                             publish__day=day
                             )
    return render(request, 'blog/post/post_detail.html', {'post': post, 'meta': post.as_meta()})

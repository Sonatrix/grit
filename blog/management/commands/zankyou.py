import feedparser
import requests

from time import mktime
from datetime import datetime
from urllib.parse import urlparse
from bs4 import BeautifulSoup 
from locator.lib.utils import safe_html, striphtml
from django.utils.text import slugify
from django.core.management.base import BaseCommand

from blog.models import Post
from locator.models import Category
from django.contrib.auth.models import User

class Command(BaseCommand):
    args = ''
    help = 'Fetch list of blogs from rss feed'

    def handle(self, **options):

        feed = feedparser.parse('https://www.zankyou.co.in/feed')

        for post in Post.objects.all().filter(sender="zankyou"):
            post.delete()
        
        author = User.objects.get(pk=1)
        loop_max = len(feed['entries'])
        category = Category.objects.get(name="Ethnic Wear")

        for i in range(0, loop_max):
            if feed['entries'][i]:
                blog_post = Post()
                blog_post.title = feed['entries'][i].title
                blog_post.slug = slugify(feed['entries'][i].title)
                blog_post.external_url = feed['entries'][i].link
                blog_post.category = category
                blog_post.sender = "zankyou"
                blog_post.author = author
                blog_post.tags = [feed['entries'][i].category]
                blog_post.short_description = striphtml(feed['entries'][i].description)
                blog_post.body = feed['entries'][i]["content"][0].value
                blog_post.created = datetime.fromtimestamp(
                   mktime(feed['entries'][i].published_parsed))
                blog_post.save()
# */30 * * * * /usr/bin/python3 /home/manage.py get_post 5

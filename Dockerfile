FROM python:3.6-slim
MAINTAINER Praveen Kumar Verma <pkv121@gmail.com>
ENV PYTHONUNBUFFERED 1

ADD requirements.txt /app/
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /app/requirements.txt
RUN pip install psycopg2-binary

ADD . /app
WORKDIR /app

ARG STATIC_URL

RUN SECRET_KEY=34ftg78%$jk@# \
    STATIC_URL=${STATIC_URL} \
    python3 manage.py collectstatic --no-input

RUN useradd --system locator && \
    mkdir -p /app/media /app/static && \
    chown -R locator:locator /app/

RUN chmod 777 entry-server.sh

USER locator

EXPOSE 8000
ENV PORT 8000

ENV PYTHONUNBUFFERED 1
ENV PROCESSES 4



